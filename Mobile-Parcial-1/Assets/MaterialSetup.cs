using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSetup : MonoBehaviour
{
    public Texture2D displacementMap;
    public Texture2D normalMap;
    public Texture2D specularMap;
    public Texture2D ambientOcclusionMap;

    void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        Material material = renderer.material;

        // Asignación de las texturas
        material.SetTexture("_DisplacementMap", displacementMap);
        material.SetTexture("_BumpMap", normalMap); // Normal map en el canal de bump map
        material.SetTexture("_SpecGlossMap", specularMap); // Especular map en el canal Specular
        material.SetTexture("_OcclusionMap", ambientOcclusionMap);

        // Opcional: Ajuste de otros parámetros del material, por ejemplo:
        // material.SetFloat("_Glossiness", 0.5f); // Ajuste de la rugosidad especular
        // material.SetFloat("_Metallic", 0.5f); // Ajuste de la cantidad de metal

        renderer.material = material;
    }
}
