Shader "Custom/NewSurfaceShader"
{
    Properties {
        _DisplacementMap ("Displacement Map", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _SpecularMap ("Specular Map", 2D) = "white" {}
        _AmbientOcclusionMap ("Ambient Occlusion Map", 2D) = "white" {}
    }

    FallBack "Diffuse"
}