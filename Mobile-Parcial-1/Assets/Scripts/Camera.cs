using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Camera : MonoBehaviour
{
    private CinemachineVirtualCamera _camera;
    private CinemachineConfiner _confiner;

    public GameObject _player;
    public GameObject llaveAnimacion;
    public Collider2D limite2;
    

    private void Start()
    {
        _camera = GetComponent<CinemachineVirtualCamera>();
        _confiner = GetComponent<CinemachineConfiner>();
    }

    public void Update()
    {
        if (Door.inAnimation)
        {
            _camera.Follow = llaveAnimacion.transform;
            _confiner.m_BoundingShape2D = limite2;
        }
        else
        {
            if (_camera.Follow != _player.transform)
            {
                _camera.Follow = _player.transform;
            }
        }
    }
}
