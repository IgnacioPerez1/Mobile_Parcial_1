using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Animator _animator;
    public static bool inAnimation = false;
    public GameObject door;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.Play("Llave");
        
    }

    public void Open()
    {
        Destroy(door);
        Destroy(this.gameObject);
        inAnimation = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _animator.Play("Llave_Agarrada");
        inAnimation = true;
    }
}
