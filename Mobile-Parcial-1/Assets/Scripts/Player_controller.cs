using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

public class Player_controller : MonoBehaviour
{
    public float jumpVelocity;

    private Rigidbody2D rb;
    private float dirx;
    private float moveSpeed = 20f;
    private bool isOnFloor = false;
    private Animator animator;
    private bool canJump;
    private SpriteRenderer _spriteRenderer;
    private int _lives = 5;

    private static readonly int Running = Animator.StringToHash("Running");
    private static readonly int Jumping = Animator.StringToHash("Jumping");
    private static readonly int Damage = Animator.StringToHash("Damage");

    public float maxSpeed = 10f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        dirx = Input.acceleration.x * moveSpeed;

        dirx = Mathf.Clamp(dirx, -maxSpeed, maxSpeed);

        animator.SetBool(Jumping, !isOnFloor);

        if (dirx > 1)
        {
            _spriteRenderer.flipX = true;
        }
        else if (dirx < -1)
        {
            _spriteRenderer.flipX = false;
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(dirx, rb.velocity.y);

        if (Mathf.Abs(dirx) > 1f)
        {
            if (isOnFloor)
            {
                animator.SetBool(Running, true);
                animator.SetBool(Jumping, false);
            }
        }
        else
        {
            if (isOnFloor)
            {
                animator.SetBool(Running, false);
                animator.SetBool(Jumping, false);
            }
        }
    }

    public void Jump()
    {
        if (canJump)
        {
            rb.AddForce(Vector2.up * jumpVelocity, ForceMode2D.Impulse);
            isOnFloor = false;
            canJump = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Floor"))
        {
            isOnFloor = true;
            canJump = true;
        }

        if (col.gameObject.CompareTag("Spike"))
        {
            TakeDamage();
        }
    }

    async void TakeDamage()
    {
        if (_lives > 0)
        {
            _lives -= 1;
            animator.SetBool(Damage , true);
            Debug.Log("Damage taken. Lives remaining: " + _lives);
            await Task.Delay(500);
            animator.SetBool(Damage , false);
        }
    }
}
